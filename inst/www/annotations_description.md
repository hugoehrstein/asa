
##### Since 2000 the Arabidopsis Genome Initiative has released numerous versions of the *A. thaliana* Col-0 genome, pairing TAIR genome assemblies with synonymously versioned TAIR annotations.

---

| Release | Maintained by |
| ------- | ------        |
| **2016**| **Araport11** (latest ralease)| 
| 2010    | TAIR10        | 
| 2009    | TAIR9         |  
| 2008    | TAIR8         | 
| 2007    | TAIR7         | 
| ...     | ...           |
| 2001    | TIGR1         |
| 2000    | *Nature paper*|


---

##### *Genome annotations*: the precise "start" and "stop" positions of experimentally determined (or predicted) genes and other sequence features. Annotations refer to coordinates in a reference assembly and thereby summarise past molecular genetic and biochemical insights that are extremely useful for comparison to novel -omics/NGS data.

---
  
##### Are included in this package:  

##### * The comprehensive reannotation of the Col-0 genome released by [Araport11](https://www.araport.org/data/araport11)   
##### * The miRNA annotations released by [miRBase (release 22.1)](http://www.mirbase.org/)  


