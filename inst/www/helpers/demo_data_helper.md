The DATA are coming from a recently published Arabidopsis epigenomic study: [Yang et al. (2017) *Genome Biol.*](https://www.ncbi.nlm.nih.gov/pubmed/28569170)

The raw sRNAseq data were analysed with ShortStack and the differents results are loaded here.

The ShortStack parameters used:

- 1 mismatch allowed
- multimapped reads allowed
- sRNA sizes between 18-35 nt allowed
- -u (unique mode)


- NRPD1 and NRPE1 encode the largest subunits of RNA polymerases IV and V, respectively.
These specialized RNA polymerases facilitate the biogenesis of 24-nt siRNAs derived from transposable elements (TEs) in plants.

- PKL description



