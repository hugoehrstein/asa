Equivalence:

| short name                | real name |
| ------------------------------------------------------------ | ------- |
| snRNA                                              | Small nuclear RNA |
| snoRNA                                            | Small nucleolar RNA    |
| rRNA                                                       | Ribosomal RNA   |
| miRNA                                                      | miRNA    |
| lncRNA                                                     | long non-coding RNA    |
| asRNA                                              | Antisense RNA     |
| as_lncRNA                                      |   Antisense long non-coding RNA    |
| tRNA                                                  |   Transfert RNA    |
| TE                                                     |   Transposable Element |
| TEgene                                              |   Transposable Element gene   |
| pseudogene                                     |   Pseudo gene    |
| novel_transcribed_region               |   Novel transcribed region    |
| prot_coding                                   | Protein coding    |
| other                                              | Other RNA     |

