# TODO: send it later to utils.R
get_sRNA_range <- function(shortstack.dt){
  # TODO check input for integrity
  col_num <- grep("^\\d+$", names(shortstack.dt))
  return(range(as.numeric(names(shortstack.dt)[col_num])))
}

melt_read_length <- function(read_length.dt){
  # TODO check input header
  #if("Size" %in% colnames(read_length.dt))
  melt(read_length.dt, id.vars = "Size", variable.name = "Filename", value.name = "Count")
}

get_row_sum <- function(shortstack.data, new_colname = "Sum", columns_to_sum = NULL) {
  # Example: getRowSum, new_colname = "Sum21-22nt", columns_to_sum = c("21", "22"), new_extension = ".21-22nt.txt")

  # TODO: add assertion + check input parameters + add import path file check + check extension
  dt[ ,(new_colname) := rowSums(.SD), .SDcols = columns_to_sum]
  dt.subset <- dt[, c("Name", new_colname), with = FALSE]
  #fwrite(dt.subset, file = gsub(".txt", new_extension, shortstack.data), col.names = FALSE, sep = "\t")
}

get_library_size <- function(read_lenght.dt, sizes = NULL, ...){
  if(sizes){
    dt <- melt_read_length(read_lenght.dt)[Size %in% sizes, list(Library_size = sum(Count)), by = c("Sample")]
    return(dt)
  } else {
    dt <- melt_read_length(read_lenght.dt)[, list(Library_size = sum(Count)), by = c("Sample")]
    return(dt)
  }
}

getOverlap <- function(regions, annotations, type = "any", ...) {
  if(!all(c("Chr", "Start", "End") %in% names(regions))) stop("Regions table should have: Chr, Start, End columns.")
  # TODO check input format of user's annotation

  #if(!"pos" %in% names(data)) stop("data table should have a pos")
  #if(!.id %in% names(regions)) stop("You should use an unique .id column in your table regions")
  annotations[, Start.annot := as.integer(Start.annot)]
  annotations[, End.annot := as.integer(End.annot)]
  # The KEYS are the columns used by the foverlaps function to make the merge!
  setkey(annotations, Chr.annot, Start.annot, End.annot)
  setkey(regions, Chr, Start, End)
  print(annotations)
  # REQUIRED to reorder the columns name after merging because annotations column appears first...
  dt <- foverlaps(regions, annotations,
                  type = type, ...)

  setcolorder(dt, names(regions))
  # TODO add function to get overlap size
  #dt[, overlap_size := overlap_bp]
  return(dt)
}
