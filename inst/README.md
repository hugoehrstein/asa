# README ASA #

# Cahier des charges



**Les différents onglets :**

Les onglets suivants sont ceux que l&#39;on retrouve dans cet ordre dans le menu sur la gauche. Certains peuvent être divisés en sous-menus pour plus de facilité.

Home :

C&#39;est la page d&#39;accueil de l&#39;application. On y retrouve une présentation de l&#39;application. Mais on y retrouve aussi la marche à suivre étape par étape afin de réduire au minimum les erreurs possibles et donc la perte de temps.

Cela doit être clair et précis pour ne pas obliger l&#39;utilisateur à chercher de l&#39;aide ailleurs.


Import :

Il y a plusieurs solutions pour importer les différents fichiers nécessaires. Une première méthode qui semble la plus simple à faire fonctionner et l&#39;import d&#39;un seul fichier préalablement traité. Ce fichier est en fait composé des différents résultats issus de ShortStack et la première colonne comporte le nom du Sample. Il est donc ensuite facile de redécouper ce fichier en différentes tables. 
Une seconde méthode est l&#39;utilisation d&#39;une Sample Table qui est préparée en amont par l&#39;utilisateur, mais pour le moment, il semble compliqué d&#39;utiliser cette méthode sur une application stockée sur un serveur.
Une autre méthode est l&#39;utilisation du package ShinyTree (1)  qui permet de se déplacer dans l&#39;arborescence, mais il est nécessaire de vérifier s&#39;il fonctionne sur une application tournant sur un serveur.

Il est impératif lors de l&#39;import de faire de nombreux tests comme :

- --Chaque colonne correspond au bon format de donnée
- --Le bon nombre de caractère pour certaines colonnes (strand, etc.)
- --Les colonnes sans données sont remplacées par NA ?
- --Toutes les colonnes nécessaires sont présentes
- --les différents samples ont le meme nombres de lignes

Dans cet onglet, on choisit également quelles annotations sera rattachées aux données.
On peut vouloir choisir de rattaché les données de araport pour les TE mais vouloir les données d'une autre banque pour un autre locus_type. Il est donc interressant de choisir pour chaque locus_type quelle banque y est rattaché. 



(1) ShinyTree : [https://github.com/shinyTree/shinyTree](https://github.com/shinyTree/shinyTree)

(2) Data.table : [https://shiny.rstudio.com/articles/datatables.html](https://shiny.rstudio.com/articles/datatables.html)



Report :

Cet onglet permet de générer un rapport et de l'afficher au format html (il peut etre téléchargé). C&#39;est un rapport standard qui donne les informations de bases avec des graphiques basiques et des options non-modifiables (couleurs, tailles, etc). Il peut être intéressant de rendre ce rapport un minimum réactif avec l&#39;utilisation de graphique généré par plotly par exemple.

Il doit être une piste pour pouvoir se concentrer sur des points plus précis.

On y retrouve des informations comme le nombre de chaque locus\_type, le nombre de chaque Size, des graphiques de RPM en fonction de Size, Count en fonction de locus\_type, RPM en fonction de Sample + Locus\_type, etc.

vizualisation:

Il est ensuite nécessaire de pouvoir visualiser les datas dans un tableau généré par le package Data.Table (2) afin que l&#39;utilisateur puisse vérifier ses données, mais également pour en tirer des informations basiques avec le package Sparkline (1) par exemple qui créé de petits graphiques très rapidement. Un onglet avec jbrowse serait un plus pour pouvoir visualiser directement le resultat d'une ligne que l'on selectionne. 

L&#39;utilisateur pourra remplir une sample table dans un onglet. Pour chaque Sample, l&#39;utilisateur remplira des colonnes d&#39;information (Nom du Sample, répliqua, etc.) et il y aura une colonnes comportant aussi le RPM pour chaque sample. L&#39;utilisateur pourra télécharger ensuite cette table pour pouvoir recommencer des analyses ultérieures sans avoir à refaire l&#39;opération précédente.

(1) Sparkline : [http://bart6114.github.io/sparklines/](http://bart6114.github.io/sparklines/)

Graph :

L&#39;onglet graph doit comporter tous les graphiques nécessaires à la compréhension totales du sujet. Il sera diviser en plusieurs sous-menus en fonction du « thème » des graphiques. Mais il y a des règles que l&#39;on respectera pour tous les graphiques :

- --La légende doit être présente
- --Les noms des différents éléments sont placés verticalement pour éviter qu&#39;ils se superposent
- --Les couleurs doivent être modifiables facilement
- --Les graphiques doivent être téléchargeable facilement (on utilise le package Plotly (1) pour cela)

Il faudrait un menu qui regroupe les options modifiables pour chaque graphique comme :

- --la palette de couleurs
- --l&#39;orientation des labels
- --modifier le titre ?



(1) Plotly : [https://plot.ly/r/shiny-gallery/](https://plot.ly/r/shiny-gallery/)


Contact :

Cette page permet de contacter facilement la personne qui s&#39;occupe de l&#39;application.

Error tab (droite):

L&#39;endroit où l&#39;on peut trouver facilement de l&#39;aide en cas d&#39;erreurs. En effet, lorsqu&#39;une erreur courante apparaît, elle comportera un « Numéro d&#39;erreur » et il suffit de renseigner ce numéro d&#39;erreur pour avoir une explication simple pour résoudre cette erreur soit même et si l&#39;utilisateur n&#39;est pas capable de résoudre l&#39;erreur, il pourra préciser le numéro de l&#39;erreur à la personne qui s&#39;occupe de l&#39;application pour savoir rapidement pourquoi on obtient cette erreur.

[https://shiny.rstudio.com/reference/shiny/1.2.0/validate.html](https://shiny.rstudio.com/reference/shiny/1.2.0/validate.html)

[https://shiny.rstudio.com/articles/validation.html](https://shiny.rstudio.com/articles/validation.html)

[https://shiny.rstudio.com/articles/modal-dialogs.html](https://shiny.rstudio.com/articles/modal-dialogs.html)


**Les étapes :**

1. 1)Finir la fonction « Import »
2. 2)Traiter les données, ajouter les données utiles (RPM, etc.), liens Jbrowse ;
3. 3)Faire les premiers graphiques avec des fonctions pour les couleurs
4. 4)Ajout de tous les graphiques nécessaires
5. 5)Gestion des erreurs

\*\*\* Première Phase de Test en local \*\*\*

Pendant la phase de test :

1. 1)Amélioration du code
2. 2)Amélioration du visuel
3. 3)Rédactions/traductions des différents textes, messages d&#39;erreur, etc
4. 4)Ajout de petites fonctionnalités

\*\*\* Retour de la première Phase de test \*\*\*

1. 1)Corrections des erreurs retournées (grâce aux numéros des erreurs)
2. 2)Ajouts des fonctionnalités demandées
3. 3)Déploiement sur le serveur

\*\*\* Seconde Phase de Test sur serveur \*\*\*

**La division du programme :**

Le plus simple pour se retrouver facilement dans les différentes fonctions c&#39;est de diviser le programme en différents fichiers qui seront appelé dans le programme principal. Une des méthodes la plus logique et de diviser le programme en suivant les différents onglets définis au-dessus. Ainsi tout les parties du programme qui génère la partie UI (User Interface) seront stocker dans un dossier nommée UI alors que les parties utiliser coté Server seront logiquement stocker dans un dossier Server. En cas d&#39;erreur il est donc facile de savoir quelle partie est impactée.

Tous les éléments supplémentaires nécessaires au fonctionnement de l&#39;application comme les images seront stockés dans le dossier www alors que les données seront stockées dans un dossier nommé bank.

**Le visuel :**

On utilise le package ShinyDashboard (1) ainsi que ShinyDashboardPlus (2) afin de générer un visuel facile d&#39;utilisation avec des sous menu.

1. (1)ShinyDashboard : [https://rstudio.github.io/shinydashboard/](https://rstudio.github.io/shinydashboard/)
2. (2)ShinyDashboardPlus : [https://github.com/RinteRface/shinydashboardPlus](https://github.com/RinteRface/shinydashboardPlus)

Le nom de l&#39;application : ASA pour Analyzer for SmallRna&#39;s Annotation

Charte graphique : L&#39;application sera majoritairement de couleurs verte pour faire un rappel du sujet principalement étudié, les plantes.
