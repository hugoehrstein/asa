tabItem(tabName = "import",

# I. Import ---------------------------------------------------------------

        tabsetPanel(
          tabPanel(
            "A. Datasets import",
            icon = icon("file-download"),
            fluidRow(
              br(),
              box(
                title = "Import Data :",
                width = 6,
                status = "success",
                solidHeader = TRUE,
                collapsible = TRUE,
                fileInput(
                  inputId = "csvs",
                  label = "Upload your ShortStack file(s) :",
                  multiple = TRUE,
                  placeholder = "Example: 'Results.txt'",
                  accept = c("text/plain", "text/tsv", "text/csv"),
                ) %>%
                  helper(
                    type = "inline",
                    title = "Inline Help",
                    content = c("This is where you import your different shortstack files."),
                    size = "l"
                  ),
                checkboxInput(
                  inputId = "demo_upload",
                  label = "Load/unload demo data",
                  value = FALSE
                ) %>%
                  helper(
                    type = "markdown",
                    title = "Demo data",
                    content = "demo_data_helper",
                    size = "l"
                  ),
                hr(),
                # tags$div(id = 'placeholder'),
                # tags$div(id = 'placeholder2')
                uiOutput("select_sRNA_sizes"),
                uiOutput("select_sRNA_sizes2")
              ),
              box(
                title = "Check :",
                width = 6,
                status = "info",
                solidHeader = TRUE,
                collapsible = TRUE,
                uiOutput("input_dt.summary")
              )
            ),
            fluidRow(
              box(
                title = "Data preview :",
                width = 12,
                status = "danger",
                solidHeader = TRUE,
                collapsible = TRUE,
                DT::dataTableOutput("input_dt.display")
              )
            )
          ),


# II. Sample table --------------------------------------------------------

          tabPanel("B. Sample Table", icon = icon("sitemap"),
                   fluidRow(column(
                     width = 12,
                     br(),
                     box(
                       title = "Sample Table :",
                       width = 3,
                       status = "success",
                       solidHeader = TRUE,
                       collapsible = TRUE,
                       selectInput(
                         inputId = "sampletable_selector",
                         label = "Load your sample table",
                         selected = "Import a sample table",
                         choices = c("Import a sample table", "New sample table", "Demo sample table")
                       ) %>%
                         helper(
                           type = "inline",
                           title = "Inline Help",
                           content = c(
                             "This is where you choose to use the sample table provided or import your own sample table.",
                             "YourST is a Sample Table prepared in ammont or a Sample Table download from the application (separator: , )",
                             "NewST is a sample table provided for first use."
                           ),
                           size = "l"
                         ),
                       hr(),
                       conditionalPanel(
                         condition = "input.sampletable_selector == 'Import a sample table'",
                         fileInput(
                           inputId = "sampletable",
                           label = "Upload your sample table",
                           multiple = FALSE,
                           placeholder = "Example: 'Results.txt'",
                           accept = c("text/plain", "text/tsv", "text/csv")
                         )
                       )
                     ),
                     box(
                       title = "Sample Table Visualization :",
                       width = 9,
                       status = "success",
                       solidHeader = TRUE,
                       collapsible = TRUE,
                       h5(
                         ""
                       )%>%
                         helper(
                           type = "inline",
                           title = "Inline Help",
                           content = c(
                             "Vous pouvez modifier la table en double cliquant sur une case. Vous pouvez aussi télécharger la sample table si vous voulez la réutiliser plus tard ( conseillé)"
                           ),
                           size = "l"
                         ),
                       #DT::dataTableOutput('sampletablevisualize')
                       rHandsontableOutput("sampletablevisualize") %>% withSpinner(size = 0.5),
                       br(),
                       actionButton(inputId = "update_sample_table", label = "Save modification(s)", icon = icon("sync"))
                     )
                     )
                   ),
                   fluidRow(
                     box(
                       title = "Data preview :",
                       width = 12,
                       status = "danger",
                       solidHeader = TRUE,
                       collapsible = TRUE,
                       #div(style = 'overflow-x: scroll', DTOutput("full_dt.display"))
                       DTOutput("full_dt.display")
                     )
                   )
          ),

# III. Annotation ---------------------------------------------------------

tabPanel("C. Annotation", icon = icon("pen-fancy"),
         fluidRow(column(
           width = 12,
           br(),
           box(
             title = "Annotation :",
             status = "success",
             solidHeader = TRUE,
             collapsible = TRUE,
             width = 3,
             selectInput(
               inputId = "AnnotationType",
               label = "Choose your annotations",
               selected = "Default annotations",
               multiple = FALSE,
               choices = c("Default annotations", "Your annotations")
             ),
             uiOutput("annotations_checkbox") %>%
               helper(
                 type = "markdown",
                 content = "equivalence",
                 title = "Inline Help",
                 size = "l"
               ),
             hr(),
             awesomeRadio(
               inputId = "annot.overlap.method",
               label = "How to overlap your data with the annotations?",
               choices = c("No annotations needed",
                           "By coordinates (Chr, Start, End columns)",
                           "By name/AGI (Name/AGI columns)",
                           "By locus (Locus column)"),
               selected = ("No annotations needed"),
               checkbox = TRUE
             ),
             awesomeCheckbox(
               inputId = "annot.overlap.removeNA",
               label = "Show only columns with a match?",
               value = FALSE
             ),
             conditionalPanel(
               condition = "input.AnnotationType == 'YourAnnotation'",
               fileInput("Annotation", label = "Upload multiple CSV here", multiple = TRUE),
               hr(),
               selectInput(
                 "sep2",
                 label = h3("Separator"),
                 choices = list(
                   "tabulation" = "\t",
                   "comma" = "," ,
                   "semicolon" = ";"
                 )
               )
             )
           ),
           tabBox(
             id = "annotations.tabbox",
             title = "Annotations",
             width = 9,
             tabPanel("Available annotations view", uiOutput("annotations_tabs")),
             tabPanel("Your data and annotations overlaps",
                      withSpinner(DT::dataTableOutput("complete.display")),
                      hr(),
                      downloadButton('downloadDataWithAnnotation', 'Download')),
             tabPanel("Graph of overlaps", icon = icon("bar-chart-o"),
                      htmlOutput("shortstack_overlap_annotations.plots2"),
                      #plotOutput("shortstack_overlap_annotations.plots"),
                      plotOutput("shortstack_overlap_annotations.stats"))
           )
         ))),

# IV. Read length ---------------------------------------------------------

         tabPanel("D. Read length", icon = icon("table"),
                   fluidRow(
                     column(width = 3,
                     br(),
                     box(
                       title = "Read length :",
                       width = 12,
                       status = "success",
                       solidHeader = TRUE,
                       collapsible = TRUE,
                       # RAW READS SELECT INPUT (DEMO LOADED AS DEFAULT)
                       selectInput(
                         inputId = "raw_reads_length_selector",
                         label = "Your raw reads length file",
                         selected = "Demo raw reads table",
                         choices = c("Upload raw reads table", "Demo raw reads table")
                       ) %>%
                         helper(
                           type = "inline",
                           title = "Inline Help",
                           content = c("Import your differents read length files"),
                           size = "l"
                         ),
                       conditionalPanel(
                         condition = "input.raw_reads_length_selector == 'Upload raw reads table'",
                         fileInput(inputId = "raw_reads_length",
                                   label = "Upload your raw reads length table",
                                   multiple = FALSE)
                         ),
                       hr(),
                       selectInput(
                         inputId = "aln_reads_length_selector",
                         label = "Your aligned reads length file",
                         selected = "Demo aln reads table",
                         choices = c("Upload aln reads table", "Demo aln reads table")
                         ) %>%
                         helper(
                           type = "inline",
                           title = "Inline Help",
                           content = c("Import your differents read length files"),
                           size = "l"
                           ),
                       conditionalPanel(
                         condition = "input.aln_reads_length_selector == 'Upload aln reads table'",
                         fileInput(inputId = "aln_reads_length",
                                   label = "Upload your aln reads length table",
                                   multiple = FALSE)
                         )
                     )
                    ),
                  column(
                    width = 9,
                    br(),
                    box(
                      title = "Raw reads length table :",
                      width = 12,
                      status = "success",
                      solidHeader = TRUE,
                      collapsible = TRUE,
                      br(),
                      rHandsontableOutput("raw_reads_length.display") %>% withSpinner(size = 0.5),
                      br()
                    ),
                     box(
                       title = "Aligned reads length table :",
                       width = 12,
                       status = "success",
                       solidHeader = TRUE,
                       collapsible = TRUE,
                       br(),
                       rHandsontableOutput("aln_reads_length.display") %>% withSpinner(size = 0.5),
                       br()
                     )
                     )
                   )),

# V. Normalization --------------------------------------------------------

          tabPanel("E. Normalization", icon = icon("wrench"),
                   fluidRow(
                     column(
                     width = 12,
                     br(),
                     box(
                       title = "Normalization Option :",
                       width = 3,
                       status = "success",
                       solidHeader = TRUE,
                       collapsible = TRUE,
                       selectInput(
                         inputId = "NormalizationType",
                         label = "Aligned or Raw reads",
                         selected = "Normalization with Aligned",
                         multiple = FALSE,
                         choices = c("Normalization with Raw", "Normalization with Aligned")
                       )%>%
                         helper(
                           type = "inline",
                           title = "Inline Help",
                           content = c("You can choose if we use the count table of raw or aligned reads for the normalization."),
                           size = "l"
                         ),
                       hr(),
                       selectInput(
                         inputId = "NormalizationSize",
                         label = "All size or Selected size ",
                         selected = "Selected Size",
                         multiple = FALSE,
                         choices = c("All Size", "Selected Size")
                       )%>%
                         helper(
                           type = "inline",
                           title = "Inline Help",
                           content = c("You can choose if we use all the sizes for the nomalization or only the selected sizes."),
                           size = "l"
                         )
                     ),
                     box(
                       title = "Data preview :",
                       width = 9,
                       status = "danger",
                       solidHeader = TRUE,
                       collapsible = TRUE,
                       DT::dataTableOutput("complete.normalized.display") %>% withSpinner(size = 0.6)
                     )
                   )
                   ))
        ))
