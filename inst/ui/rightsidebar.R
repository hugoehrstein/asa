rightsidebar = rightsidebar = rightSidebar(
  background = "dark",
  rightSidebarTabContent(
    id = 1,
    icon = "question-circle",
    title = tags$h1("Helper"),
    active = TRUE,
    hr(),
    h5(
      "You can choose the number of the displayed error and help will appear."
    ),
    numericInput(
      "error2",
      label = h4("Error number :"),
      value = 0,
      min = -1
    ),
    hr(),
    
    conditionalPanel(
      condition = "input.error2 == '-1'",
      h1("Special Thanks"),
      h5(
        "A
        big tha
        nk
        you t
        o the bioi
        nformat
        ics tea
        m at ib
        mp and esp
        ecially to Va
        lerie Cog
        nat and Dav
        id Pfli
        eger!"
      )
      ),
    
    
    conditionalPanel(condition = "input.error2 == '1'",
                     h1("Error 1"), h5("This first error is...")),
    
    conditionalPanel(
      condition = "input.error2 == '2'",
      h1("Error 2"),
      h5(
        "There is not the same number of lines between the imported
        file and the merged file. The difference is"
      ),
      textOutput("diff"),
      h5("row.")
      ),
    
    conditionalPanel(condition = "input.error2 == '3'",
                     h1("Error 3"), h5("This third error is...")),
    
    conditionalPanel(condition = "input.error2 == '4'",
                     h1("Error 4"), h5("This fourth error is..."))
    
      ),
  
  
  rightSidebarTabContent(
    id = 2,
    icon = "user-cog",
    title = tags$h1("Options :"),
    hr(),
    
    textInput("name", label = h3("Name input"), value = "Enter your name..."),
    
    hr(),
    sliderTextInput(
      inputId = "saveformat",
      label = "Gaph format export:",
      grid = TRUE,
      force_edges = TRUE,
      choices = c("eps", "ps", "tex", "pdf", "jpeg", "tiff", "png", "bmp", "svg"),
      selected = "svg"
    )        %>%
      helper(
        type = "inline",
        title = "Inline Help",
        colour = "white",
        content = c("You can choose once the export format of all the ggplot graphs."),
        size = "l"
      ),
    hr(),
    selectInput(
      "jbrowseoption",
      "jbrowse Option:",
      selected = "Defautjbrowse",
      c(Yourjbrowse = "Yourjbrowse", Defautjbrowse = "Defautjbrowse")
    )        %>%
      helper(
        type = "inline",
        title = "Inline Help",
        colour = "white",
        content = c("This is where you import your different annotation files."),
        size = "l"
      ),
    conditionalPanel(
      condition = "input.jbrowseoption == 'Yourjbrowse'",
      textInput(
        "linkjbrowse",
        label = h3("your jbrowse link"),
        value = "Enter link..."
      )
      
    )
    
  )
  
    )