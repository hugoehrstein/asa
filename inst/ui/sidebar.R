dashboardSidebar(
  h4("Menu", align = "center"),
  sidebarMenu(
    menuItem("Home", tabName = "home", icon = icon("home")),
    menuItem(
      "I. Import",
      tabName = "import",
      icon = icon("file-import")
    ),
    menuItem(
      "II. preliminary report",
      tabName = "report",
      icon = icon("file-invoice")
    ),
    menuItem(
      "III. Visualization",
      icon = icon("table"),
      menuSubItem("1. Data", tabName = "visualize"),
      menuSubItem("2. Jbrowse viewer", tabName = "jbrowse")
    ),
    menuItem(
      "IV. Graph",
      icon = icon("chart-bar"),
      menuSubItem("1. Boxplot", tabName = "Boxplot"),
      menuSubItem("2. Histogram", tabName = "Histogram"),
      menuSubItem("3. Heatmap", tabName = "Heatmap"),
      menuSubItem("4. Other Plot", tabName = "otherplot")
    ),
    menuItem(
      "V. Export",
      tabName = "export",
      icon = icon("file-export")
    ),
    menuItem(
      "Contact",
      tabName = "contact",
      icon = icon("address-card")
    ),
    bookmarkButton(label = "Save state"),


    helpText(
      "Developed by Hugo Ehrstein",
      br(),
      "under the direction of",
      br(),
      "Valérie Cognat",
      "for the ",
      a("IBMP", href = "http://www.ibmp.cnrs.fr", target="_blank"),
      style = "padding-left:1em; padding-right:1em;position:absolute; bottom:1em; "
    )

  )
)
